﻿using System;
using System.Windows;
using System.Windows.Input;


namespace myPlayer
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private void Slider_play_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (clicSlider == true && !_sliderpressed)
            {
                try
                {
                    //int slidervalue = (int)Slider_play.Value;
                    //TimeSpan ts = new TimeSpan(0, 0, 0, 0, slidervalue);
                    //mediaPlayer.Position = ts;
                }
                catch (Exception)
                {
                    Current.Text = "fail Value Changed slider play\n";
                }
            }
            clicSlider = true;
        }

        private void Slider_Sound_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            try
            {
                mediaPlayer.Volume = (double)Slider_Sound.Value;
            }
            catch (Exception)
            {
                Current.Text = "fail Slider sound Value changed\n";
            }
        }

        private void Slider_play_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            inBox = true;
            _sliderpressed = true;
            enterslidervalue = (int)Slider_play.Value;
        }

        private void Slider_play_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            inBox = false;
            int slidervalue = (int)Slider_play.Value;
            if (slidervalue != enterslidervalue)
            {
                TimeSpan ts = new TimeSpan(0, 0, 0, 0, slidervalue);
                mediaPlayer.Position = ts;
            }
            _sliderpressed = false;
        }

        private void Slider_Sound_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (e.Delta > 0)
            {
                Slider_Sound.Value = (Slider_Sound.Value + 0.01 > 1) ? (1) : (Slider_Sound.Value + 0.01);
                mediaPlayer.Volume = (double)Slider_Sound.Value;
            }
            else if (e.Delta < 0)
            {
                Slider_Sound.Value = (Slider_Sound.Value - 0.01 < 0) ? (0) : (Slider_Sound.Value - 0.01);
                mediaPlayer.Volume = (double)Slider_Sound.Value;
            }
        }
    }
}
