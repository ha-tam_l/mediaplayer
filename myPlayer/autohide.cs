﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Windows.Forms;

namespace myPlayer
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private bool isShown = true;
        bool inBox = false;
        private System.Windows.Visibility PL;
        double pX = 0;
        double pY = 0;

        private void hideAll()
        {
            if (isShown == true && inBox == false && isplaying == true)
            {
                PL = playlistGrid.Visibility;
                mediaPlayer.SetValue(Grid.RowProperty, 0);
                mediaPlayer.SetValue(Grid.ColumnSpanProperty, 2);
                mediaPlayer.SetValue(Grid.RowSpanProperty, 3);

                subtileArea.SetValue(Grid.RowProperty, 0);
                subtileArea.SetValue(Grid.ColumnSpanProperty, 2);
                subtileArea.SetValue(Grid.RowSpanProperty, 3);

                playlistGrid.Visibility = System.Windows.Visibility.Hidden;
                playlistSplitter.Visibility = System.Windows.Visibility.Hidden;
                ButtonList.Visibility = System.Windows.Visibility.Hidden;
                Exit.Visibility = System.Windows.Visibility.Hidden;
                Mini.Visibility = System.Windows.Visibility.Hidden;
                Maxi.Visibility = System.Windows.Visibility.Hidden;
                Current.Visibility = System.Windows.Visibility.Hidden;
                GridLower.Visibility = System.Windows.Visibility.Hidden;
                mediaPlayer.Cursor = System.Windows.Input.Cursors.None;
                isShown = false;
            }
        }

        private void showAll()
        {
            if (isShown == false)
            {
                mediaPlayer.SetValue(Grid.RowProperty, 1);
                mediaPlayer.SetValue(Grid.RowSpanProperty, 1);
                subtileArea.SetValue(Grid.RowProperty, 1);
                subtileArea.SetValue(Grid.RowSpanProperty, 1);
                if (PL == System.Windows.Visibility.Visible)
                {
                    mediaPlayer.SetValue(Grid.ColumnSpanProperty, 1);
                    subtileArea.SetValue(Grid.ColumnSpanProperty, 1);
                    playlistGrid.Visibility = System.Windows.Visibility.Visible;
                    playlistSplitter.Visibility = System.Windows.Visibility.Visible;
                }
                else
                {
                    subtileArea.SetValue(Grid.ColumnSpanProperty, 2);
                    mediaPlayer.SetValue(Grid.ColumnSpanProperty, 2);
                }
                GridLower.Visibility = System.Windows.Visibility.Visible;
                ButtonList.Visibility = System.Windows.Visibility.Visible;
                Exit.Visibility = System.Windows.Visibility.Visible;
                Mini.Visibility = System.Windows.Visibility.Visible;
                Maxi.Visibility = System.Windows.Visibility.Visible;
                Current.Visibility = System.Windows.Visibility.Visible;
                mediaPlayer.Cursor = System.Windows.Input.Cursors.Arrow;
                isShown = true;
            }
        }

        DispatcherTimer timerHide = new DispatcherTimer();
        private void myWindow_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            System.Windows.Point position = e.GetPosition(this);
            double deltaX = pX - position.X;
            deltaX = (deltaX > 0) ? (deltaX) : (deltaX * -1);
            double deltaY = pY - position.Y;
            deltaY = (deltaY > 0) ? (deltaY) : (deltaY * -1);
            if (deltaX > 5 && deltaY > 5)
            {
                showAll();
                pX = position.X;
                pY = position.Y;
                timerHide.Stop();
                timerHide.Interval = TimeSpan.FromSeconds(3);
                timerHide.Tick += timerHide_Tick;
                timerHide.Start();
            }
        }

        void timerHide_Tick(object sender, EventArgs e)
        {
            hideAll();
        }

        private void button_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            inBox = true;
        }

        private void button_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            inBox = false;
        }
    }
}