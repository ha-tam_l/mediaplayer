﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Windows.Forms;
using System.IO;


namespace myPlayer
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        // a voir pour pouvoir le mettre avec le xaml etc...
        private void hidePlayList()
        {
            if (playlistGrid.Visibility == System.Windows.Visibility.Visible)
            {
                subtileArea.SetValue(Grid.ColumnSpanProperty, 2);
                mediaPlayer.SetValue(Grid.ColumnSpanProperty, 2);
                playlistGrid.Visibility = System.Windows.Visibility.Hidden;
                playlistSplitter.Visibility = System.Windows.Visibility.Hidden;
            }
        }

        private void showPlayList()
        {
            if (playlistGrid.Visibility != System.Windows.Visibility.Visible)
            {
                subtileArea.SetValue(Grid.ColumnSpanProperty, 1);
                mediaPlayer.SetValue(Grid.ColumnSpanProperty, 1);
                playlistGrid.Visibility = System.Windows.Visibility.Visible;
                playlistSplitter.Visibility = System.Windows.Visibility.Visible;
            }
        }

        private void addItem(String path)
        {
            String title;
            var item = path.Split('\\');
            title = item.Last();
            VideoList.Add(new Description { Title = title, uri = new Uri(path) });
        }

        private void addItem(String Title, Uri uri)
        {
            VideoList.Add(new Description { Title = "YouTube: " + Title, uri = uri });
        }

        private void addPlaylist_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog fDialog = new OpenFileDialog();
            fDialog.Title = "Select media file";
            fDialog.Filter = "all|*";
            fDialog.Multiselect = true;
            DialogResult result = fDialog.ShowDialog();
            try
            {
                foreach (string item in fDialog.FileNames)
                {
                    addItem(item);
                }
                Playlist.Items.Refresh();
            }
            catch (Exception)
            {
                Current.Text = "FAIL open!";
            }
        }

        private void Label_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                Description des = (Description)Playlist.SelectedItem;
                index = Playlist.SelectedIndex;

                if (des.Title != "")
                {
                    mediaPlayer.Source = des.uri;
                    mediaPlayer.LoadedBehavior = MediaState.Manual;
                    mediaPlayer.UnloadedBehavior = MediaState.Manual;
                    mediaPlayer.Volume = (double)Slider_Sound.Value;
                    PlayView();
                    mediaPlayer.Play();
                    Current.Text = des.Title;
                }

            }
            catch (Exception)
            {
                VideoList.Clear();
            }
        }

        private void exportPlaylist_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog fDialog = new SaveFileDialog();
            fDialog.Title = "Save your playlist";
            fDialog.AddExtension = true;
            fDialog.DefaultExt = "42";
            DialogResult result = fDialog.ShowDialog();
            if (fDialog.FileName == "")
                return;
            using (FileStream stream = File.Open(fDialog.FileName, FileMode.OpenOrCreate))
            using (BinaryWriter writer = new BinaryWriter(stream))
                try
                {
                    foreach (var item in VideoList)
                    {
                        String path = item.uri.AbsolutePath;
                        String title = item.Title;
                        writer.Write(path + ";" + title + "\n");
                    }
                }
                catch (Exception)
                {

                }
        }

        private void importPlaylist_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog fDialog = new OpenFileDialog();
            fDialog.Title = "Open your playlist";
            fDialog.Filter = "42 file|*.42";
            fDialog.Multiselect = false;
            DialogResult result = fDialog.ShowDialog();
            if (fDialog.FileName == "")
                return;

            using (StreamReader sr = new StreamReader(fDialog.FileName))
            {
                String line = sr.ReadToEnd();
                var array = line.Split('\n');
                foreach (var item in array)
                {
                    try
                    {
                        Description desc = new Description();
                        desc.uri = new Uri(item.Split(';')[0].Remove(0, 1));
                        desc.Title = item.Split(';')[1];
                        VideoList.Add(desc);
                        Playlist.Items.Refresh();
                    }
                    catch (Exception)
                    {
                    }
                }
            }
        }

        private void delPlaylist_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                index = Playlist.SelectedIndex;
                if (index == -1)
                    return;
                VideoList.RemoveAt(index);
                Playlist.Items.Refresh();
            }
            catch (Exception)
            {
            }
        }

        private void toPlaylist_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var tracks = Library.SelectedItems;
                foreach (var item in tracks)
                {
                    addItem(((LibrabyTracks)item).Path);
                }
                Playlist.Items.Refresh();
            }
            catch (Exception)
            {
            }
        }

    }

    public class Description
    {
        public string Title { get; set; }
        public Uri uri { get; set; }
    }

}