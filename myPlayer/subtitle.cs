﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace myPlayer
{
    class subtitle
    {
        public bool toShow { get; set; }
        private List<DataSubtitle> _subtitle;
        public subtitle()
        {
            toShow = false;
            _subtitle = new List<DataSubtitle>();
        }

        public async Task<bool> open()
        {
            OpenFileDialog fDialog = new OpenFileDialog();
            fDialog.Title = "Select media file";
            fDialog.Filter = "srt|*.srt";
            fDialog.Multiselect = false; // pour l'instant
            DialogResult result = fDialog.ShowDialog();
            try
            {
                if (fDialog.FileName != "")
                {
                    _subtitle.Clear();
                    String extention = Path.GetExtension(fDialog.FileName);
                    if (extention != ".srt")
                        throw new Exception();
                    return await ReadFile(fDialog.FileName);
                }
            }
            catch (Exception)
            {
                Console.WriteLine("FAIL open subtitle!");
                return false;
            }
            return true;
        }

        private String getSub(TimeSpan time)
        {
            foreach (var item in _subtitle)
            {
                if (item.start < time && item.end > time)
                    return item.data;
            }
            return "";
        }

        public async Task<String> getSubtitleAsync(TimeSpan time)
        {
            return await Task.Run(() => getSub(time));
        }

        private async Task<bool> ReadFile(String path)
        {
            try
            {
                using (StreamReader sr = new StreamReader(path))
                {
                    String line = await sr.ReadToEndAsync();
                    parseSubtitle(line);
                }
            }
            catch (Exception)
            {
                Console.WriteLine("FAIL load subtitle.");
                return false;
            }
            return true;
        }

        private void parseSubtitle(String data)
        {
            var array = data.Split('\n');
            int i;
            int len = array.Length;
            for (i = 0; i < len; ++i)
            {
                DataSubtitle sub = new DataSubtitle();
                sub.id = Convert.ToInt32(array[i++]);
                var timer = array[i++].Split(' ');
                timer[0].Replace(',', '.');
                timer[2].Replace(',', '.');
                sub.start = TimeSpan.Parse(timer[0]);
                sub.end = TimeSpan.Parse(timer[2]);
                while (i < len && array[i].Length > 1)
                {
                    sub.data += array[i];
                    ++i;
                }
                _subtitle.Add(sub);
            }
        }
    }

    class DataSubtitle
    {
        public int id { get; set; }
        public TimeSpan start { get; set; }
        public TimeSpan end { get; set; }
        public String data { get; set; }

        public DataSubtitle()
        {
            data = "";
        }
    }
}
