﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace myPlayer
{
   public partial class MainWindow : Window
    {

       public void PlayView()
       {
           isplaying = true;
           ispaused = false;
           buttonPlay.Visibility = Visibility.Collapsed;
           buttonPause.Visibility = Visibility.Visible;
       }

       public void PauseView()
       {
           isplaying = false;
           ispaused = true;
           buttonPlay.Visibility = Visibility.Visible;
           buttonPause.Visibility = Visibility.Collapsed;
       }
    }
}
