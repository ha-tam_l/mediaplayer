﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using MyToolkit.Multimedia;


namespace myPlayer
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        async void YouTubeVideo(String id)
        {
            try
            {
                Current.Text = "Playing : [" + id + "]";
                var youtubeurl = await YouTube.GetVideoUriAsync(id, YouTubeQuality.Quality1080P);
                
                if (youtubeurl != null)
                {
                    addItem(id, youtubeurl.Uri);
                    mediaPlayer.Source = youtubeurl.Uri;
                    mediaPlayer.LoadedBehavior = MediaState.Manual;
                    mediaPlayer.UnloadedBehavior = MediaState.Manual;
                    PlayView();
                    mediaPlayer.Play();
                }
            }
            catch (Exception)
            {
                Current.Text = "Error : Video Not Found";
            }
        }

        private void YouTubePlay_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                YouTubeVideo(YouTubePlay.Text);
                YouTubePlay.Text = "YouTube ID";
            }
        }

        private void YouTubePlay_GotFocus(object sender, RoutedEventArgs e)
        {
            YouTubePlay.Text = "";
        }
    }
}