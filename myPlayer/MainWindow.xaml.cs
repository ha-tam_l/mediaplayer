﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using System;
using System.IO;
using TagLib;
using System.Windows.Forms;
using System.Windows.Media.Imaging;


namespace myPlayer
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private DispatcherTimer m_timer;
        private bool isplaying = false;
        private bool ispaused = false;
        private bool clicSlider = false;
        private bool _sliderpressed = false;
        private int enterslidervalue;
        List<Description> VideoList = new List<Description>();
        List<LibrabyTracks> LibraryList = new List<LibrabyTracks>();
        subtitle subData = new subtitle();
        int index;
        private bool _repeat;
        //taglib  -- parse file !!

        public MainWindow()
        {
            InitializeComponent();
            m_timer = new DispatcherTimer();
            timerHide = new DispatcherTimer();
            Playlist.ItemsSource = VideoList;
            Library.ItemsSource = LibraryList;
            loadLibrary();
        }

        private void Window_Drop(object sender, System.Windows.DragEventArgs e)
        {
            try
            {
                string filename = (string)((System.Windows.DataObject)e.Data).GetFileDropList()[0];
                foreach (string item in ((System.Windows.DataObject)e.Data).GetFileDropList())
                {
                    addItem(item);
                }
                Playlist.Items.Refresh();
                mediaPlayer.Source = new Uri(filename);
                mediaPlayer.LoadedBehavior = MediaState.Manual;
                mediaPlayer.UnloadedBehavior = MediaState.Manual;
                mediaPlayer.Volume = (double)Slider_Sound.Value;
                PlayView();
                mediaPlayer.Play();
                Current.Text = filename;
            }
            catch (Exception)
            {
                Current.Text = "fail drop\n";
            }
        }

        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }

        private void repeat_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_repeat == true)
                {
                    lRepeat.Content = "no Repeat";
                }
                else
                {
                    lRepeat.Content = "Repeat";
                }
                _repeat = !_repeat;
            }
            catch (System.Exception)
            {
            }
        }

        private void GridViewColumnHeader_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                index = Library.SelectedIndex;
                if (index == -1)
                    return;
                LibrabyTracks track = (LibrabyTracks)Library.SelectedItem;
                addItem(track.Path);
                Playlist.Items.Refresh();
            }
            catch (Exception)
            {
            }
        }

    }
}
