﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Windows.Forms;
using System.IO;

namespace myPlayer
{
    public partial class MainWindow : Window
    {
        private void loadLibrary()
        {
            String[] Pictures = Directory.GetFiles(Environment.GetFolderPath(Environment.SpecialFolder.MyPictures), "*.*", SearchOption.AllDirectories);
            String[] Musics = Directory.GetFiles(Environment.GetFolderPath(Environment.SpecialFolder.MyMusic), "*.*", SearchOption.AllDirectories);
            String[] Videos = Directory.GetFiles(Environment.GetFolderPath(Environment.SpecialFolder.MyVideos), "*.*", SearchOption.AllDirectories);
            TagLib.File file = null;

            # region Pictures
            foreach (String item in Pictures)
            {
                try
                {
                    using (file = TagLib.File.Create(item))
                    {
                        if (file != null && (item.EndsWith(".png") || item.EndsWith(".jpg") || item.EndsWith(".jpeg") ||
                            item.EndsWith(".ico")))
                        {
                            LibrabyTracks track = new LibrabyTracks();
                            track.TrackType = "Picture";
                            track.Path = item;
                            track.Title = (item.Split('\\'))[item.Split('\\').Length - 1];
                            //track.Auteur = file.Tag.Performers[0];
                            //track.Album = file.Tag.Album;
                            LibraryList.Add(track);
                        }
                    }
                }
                catch (Exception)
                {
                }
            }
            #endregion

            #region Musics
            foreach (String item in Musics)
            {
                try
                {
                    using (file = TagLib.File.Create(item))
                    {
                        if (file != null && (item.EndsWith(".mp3") || item.EndsWith(".wav") || item.EndsWith(".ogg")))
                        {
                            LibrabyTracks track = new LibrabyTracks();
                            track.TrackType = "Music";
                            track.Path = item;
                            track.Title = (item.Split('\\'))[item.Split('\\').Length - 1];
                            track.Auteur = file.Tag.Performers[0];
                            track.Album = file.Tag.Album;
                            LibraryList.Add(track);
                        }
                    }
                }
                catch (Exception)
                {
                }
            }
            #endregion

            # region Videos
            foreach (String item in Videos)
            {
                try
                {
                    using (file = TagLib.File.Create(item))
                    {
                        if (file != null && (item.EndsWith(".avi") || item.EndsWith(".wmv")))
                        {
                            LibrabyTracks track = new LibrabyTracks();
                            track.TrackType = "Video";
                            track.Path = item;
                            track.Title = (item.Split('\\'))[item.Split('\\').Length - 1];
                            //track.Auteur = file.Tag.Performers[0];
                            //track.Album = file.Tag.Album;
                            LibraryList.Add(track);
                        }
                    }
                }
                catch (Exception)
                {
                }
            }
            # endregion
            Library.Items.Refresh();
        }
        // 0 equal || -1 y > x || +1 x > y
        private static int CompareLibrabyTitle(LibrabyTracks x, LibrabyTracks y)
        {
            return String.Compare(x.Title, y.Title);
        }

        private static int CompareLibrabyAlbum(LibrabyTracks x, LibrabyTracks y)
        {
            return String.Compare(x.Album, y.Album);
        }

        private static int CompareLibrabyAuteur(LibrabyTracks x, LibrabyTracks y)
        {
            return String.Compare(x.Auteur, y.Auteur);
        }

        private static int CompareLibrabyType(LibrabyTracks x, LibrabyTracks y)
        {
            return String.Compare(x.TrackType, y.TrackType);
        }

        private void GridViewColumnHeader_Click_Title(object sender, RoutedEventArgs e)
        {
            LibraryList.Sort(CompareLibrabyTitle);
            Library.Items.Refresh();
        }
        private void GridViewColumnHeader_Click_Album(object sender, RoutedEventArgs e)
        {
            LibraryList.Sort(CompareLibrabyAlbum);
            Library.Items.Refresh();
        }
        private void GridViewColumnHeader_Click_Auteur(object sender, RoutedEventArgs e)
        {
            LibraryList.Sort(CompareLibrabyAuteur);
            Library.Items.Refresh();
        }
        private void GridViewColumnHeader_Click_TrackType(object sender, RoutedEventArgs e)
        {
            LibraryList.Sort(CompareLibrabyType);
            Library.Items.Refresh();
        }


    }
    public class LibrabyTracks
    {
        public String Title { get; set; }
        public String Path { get; set; }
        public String TrackType { get; set; }
        public String Auteur { get; set; } 
        public String Album { get; set; }
    }
}
