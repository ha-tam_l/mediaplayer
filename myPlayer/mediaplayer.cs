﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace myPlayer
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private void mediaPlayer_MediaEnded(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_repeat == true)
                {
                    PlayView();
                    Slider_play.Value = 0.0;
                    mediaPlayer.SpeedRatio = 1;
                    mediaPlayer.Position = new TimeSpan(0);
                    return;
                }
                StopTimer();
                if (ispaused == true || isplaying == true)
                {
                    if (VideoList.Count > index + 1)
                    {
                        Playlist.SelectedIndex = index + 1;
                        Description des = (Description)Playlist.SelectedItem;

                        if (des != null && des.Title != "")
                        {
                            mediaPlayer.Source = des.uri;
                            mediaPlayer.LoadedBehavior = MediaState.Manual;
                            mediaPlayer.UnloadedBehavior = MediaState.Manual;
                            mediaPlayer.Volume = (double)Slider_Sound.Value;
                            PlayView();
                            Slider_play.Value = 0.0;
                            mediaPlayer.SpeedRatio = 1;
                            mediaPlayer.Play();
                            Current.Text = des.Title;
                        }
                        Current.Text = des.Title;
                    }
                    else
                    {
                        Slider_play.Value = 0.0;
                        mediaPlayer.SpeedRatio = 1;
                        PauseView();
                        mediaPlayer.Stop();
                    }

                }
            }
            catch (Exception)
            {
                Current.Text = "Fail MediaEnded\n";
            }
        }

        private void mediaPlayer_MediaFailed(object sender, ExceptionRoutedEventArgs e)
        {
            // Handle media failed
            Current.Text = "Load Fail\n";
        }

        private void mediaPlayer_MediaOpened(object sender, RoutedEventArgs e)
        {
            try
            {
                hidePlayList();
                if (Duration.Automatic != mediaPlayer.NaturalDuration)
                {
                    Slider_play.Maximum = mediaPlayer.NaturalDuration.TimeSpan.TotalMilliseconds;
                    Slider_play.LargeChange = Slider_play.Maximum / 80;
                    m_timer.Interval = TimeSpan.FromMilliseconds(((Slider_play.Maximum / 200) > 1000) ? (1000) : (Slider_play.Maximum / 200));
                    m_timer.Tick += timer_Tick;
                    m_timer.Start();
                    mediaPlayer.SpeedRatio = 1;
                    DuationText.Text = mediaPlayer.NaturalDuration.TimeSpan.ToString(@"hh\:mm\:ss");
                    PositionText.Text = mediaPlayer.Position.ToString(@"hh\:mm\:ss");
                    subData.toShow = false;
                }
                else
                {
                    Slider_play.Maximum = 0;
                    m_timer.Tick -= timer_Tick;
                    m_timer.Start();
                }
            }
            catch (Exception)
            {
                Current.Text = "fail open Media\n";
            }
        }

        async void setSubtitle(TimeSpan timer)
        {
            subtileArea.Text = await subData.getSubtitleAsync(timer);
        }

        void timer_Tick(object sender, EventArgs e)
        {
            if (!_sliderpressed)
            {
                clicSlider = false;
                try
                {
                    Slider_play.Value = mediaPlayer.Position.TotalMilliseconds;
                    PositionText.Text = mediaPlayer.Position.ToString(@"hh\:mm\:ss");
                    if (subData.toShow == true)
                        setSubtitle(mediaPlayer.Position);
                }
                catch (Exception)
                {
                    Current.Text = "fail setValue Slider Play\n";
                }
            }
        }

        private void StopTimer()
        {
            m_timer.Stop();
            m_timer.Tick -= timer_Tick;
        }

        private void mediaPlayer_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (Duration.Automatic != mediaPlayer.NaturalDuration)
                if (ispaused == true && isplaying == false)
                {
                    try
                    {
                        if (mediaPlayer.Position == new TimeSpan(0, 0, 0, 0, 0))
                        {
                            Slider_play.Maximum = mediaPlayer.NaturalDuration.TimeSpan.TotalMilliseconds;
                            Slider_play.LargeChange = Slider_play.Maximum / 80;
                            m_timer.Interval = TimeSpan.FromMilliseconds(Slider_play.Maximum / 200);
                            m_timer.Tick += timer_Tick;
                            m_timer.Start();
                        }
                        PlayView();
                        mediaPlayer.Position = new TimeSpan(0, 0, 0, 0, (int)Slider_play.Value);
                        mediaPlayer.Play();

                    }
                    catch (Exception)
                    {
                        Current.Text = "fail Play by clic\n";
                    }
                }
                else if (ispaused == false && isplaying == true)
                {
                    PauseView();
                    try
                    {
                        mediaPlayer.Pause();
                    }
                    catch (Exception)
                    {
                        Current.Text = "fail pause by clic\n";
                    }
                }
        }

        private void mediaPlayer_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (e.Delta > 0)
            {
                Slider_Sound.Value = (Slider_Sound.Value + 0.01 > 1) ? (1) : (Slider_Sound.Value + 0.01);
                mediaPlayer.Volume = (double)Slider_Sound.Value;
            }
            else if (e.Delta < 0)
            {
                Slider_Sound.Value = (Slider_Sound.Value - 0.01 < 0) ? (0) : (Slider_Sound.Value - 0.01);
                mediaPlayer.Volume = (double)Slider_Sound.Value;
            }
        }
    }
}