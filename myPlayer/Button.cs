﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Forms;
using System.Windows.Controls;

namespace myPlayer
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private void buttonPlay_Click(object sender, RoutedEventArgs e)
        {
            if (Duration.Automatic != mediaPlayer.NaturalDuration &&
                ispaused == true && isplaying == false)
            {
                try
                {
                    if (mediaPlayer.Position == new TimeSpan(0, 0, 0, 0, 0))
                    {
                        Slider_play.Maximum = mediaPlayer.NaturalDuration.TimeSpan.TotalMilliseconds;
                        Slider_play.LargeChange = Slider_play.Maximum / 80;
                        m_timer.Interval = TimeSpan.FromMilliseconds(((Slider_play.Maximum / 200) > 1000) ? (1000) : (Slider_play.Maximum / 200));
                        m_timer.Tick += timer_Tick;
                        m_timer.Start();
                    }
                    PlayView();
                    mediaPlayer.Position = new TimeSpan(0, 0, 0, 0, (int)Slider_play.Value);
                    mediaPlayer.Play();

                }
                catch (Exception)
                {
                    Current.Text = "fail Play\n";
                }
            }
        }

        private void buttonPause_Click(object sender, RoutedEventArgs e)
        {
            if (Duration.Automatic != mediaPlayer.NaturalDuration)
            {
                PauseView();
                try
                {
                    mediaPlayer.Pause();
                }
                catch (Exception)
                {
                    Current.Text = "fail pause\n";
                }
            }
        }

        private void buttonStop_Click(object sender, RoutedEventArgs e)
        {
            if (ispaused == true || isplaying == true)
            {
                try
                {
                    mediaPlayer.SpeedRatio = 1;
                    PauseView();
                    mediaPlayer.Stop();
                }
                catch (Exception)
                {
                    Current.Text = "fail Stop\n";
                }
            }
        }

        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        private void Maxi_Click(object sender, RoutedEventArgs e)
        {
            if (this.WindowState == WindowState.Maximized)
                this.WindowState = WindowState.Normal;
            else
                this.WindowState = WindowState.Maximized;
        }

        private void velocityinf_Click(object sender, RoutedEventArgs e)
        {
            var value = mediaPlayer.SpeedRatio;
            value -= 0.05;
            mediaPlayer.SpeedRatio = (value < 0) ? (0.05) : (value);
        }

        private void velocitysup_Click(object sender, RoutedEventArgs e)
        {
            var value = mediaPlayer.SpeedRatio;
            value += 0.05;
            mediaPlayer.SpeedRatio = (value > 3) ? (3) : (value);
        }


        private void ButtonList_Click(object sender, RoutedEventArgs e)
        {
            if (playlistGrid.Visibility == System.Windows.Visibility.Visible)
                hidePlayList();
            else
                showPlayList();
        }

        private void buttonOpen_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog fDialog = new OpenFileDialog();
            fDialog.Title = "Select media file";
            fDialog.Filter = "all|*";
            fDialog.Multiselect = false; // pour l'instant
            DialogResult result = fDialog.ShowDialog();
            try
            {
                if (fDialog.FileName != "")
                {
                    addItem(fDialog.FileName);
                    mediaPlayer.Source = new Uri(fDialog.FileName);
                    mediaPlayer.LoadedBehavior = MediaState.Manual;
                    mediaPlayer.UnloadedBehavior = MediaState.Manual;
                    mediaPlayer.Volume = (double)Slider_Sound.Value;
                    PlayView();
                    mediaPlayer.Play();
                    Current.Text = fDialog.FileName;
                }
            }
            catch (Exception)
            {
                Current.Text = "FAIL open!";
            }
        }

        public async void sub_clic()
        {
            if ((await subData.open()) == true)
            {
                subData.toShow = true;
            }
            else
                subData.toShow = false;
        }

        private void sub_Click(object sender, RoutedEventArgs e)
        {
            sub_clic();
        }
    }
}